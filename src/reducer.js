
const balanceState = {
    usd: 156.12,
    btc: 0,
    last_price: null,
    error: null,
    tradeAmount: null,
    quote: 0
};

const reducer = (state = balanceState, action) => {

    switch (action.type) {
        case 'SET_LAST_PRICE':
            const lastPrice = action.payload.last_price;
            return {
                ...state,
                last_price: lastPrice
            };
        case 'UPDATE_QUOTE':
            const tradeAmount = Number(action.payload.tradeAmount);
            const quote = Number((tradeAmount / state.last_price).toFixed(8));
            if (state.usd < tradeAmount) {
                return {
                    ...state,
                    quote: quote,
                    tradeAmount: tradeAmount,
                    error: 'Insufficient funds'
                }
            }
            if (!tradeAmount || tradeAmount < 0) {
                return {
                    ...state,
                    quote: quote,
                    tradeAmount: tradeAmount,
                    error: 'Trade Amount must be a valid positive number'
                }
            }
            return {
                ...state,
                tradeAmount: tradeAmount,
                quote: quote,
                error: null,
            };
        case 'EXECUTE_TRADE':
            if (state.error) {
                return state
            }
            if (state.usd < state.tradeAmount) {
                return {
                    ...state,
                    error: 'Insufficient funds'
                }
            }
            const usdBalance = Number((state.usd - state.tradeAmount).toFixed(2));
            const btcBalance = Number((state.btc + state.quote).toFixed(8));
            return {
                ...state,
                usd: usdBalance,
                btc: btcBalance,
                error: null,
            };
    }
    return state;
};

export default reducer;