import axios from 'axios';
const url = 'https://api.bitfinex.com/v2/ticker/tBTCUSD';

export function updateQuote(amount) {
    return function(dispatch) {
        axios.get(url).then((response) => {
            const payload = {
                tBTCUSD: response.data[9],
                tradeAmount: amount
            };
            dispatch({type: 'UPDATE_QUOTE', payload});
        }).catch((err) => {
            console.log(err);
            throw err;
        });
    }
}