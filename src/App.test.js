import React from 'react';
import store from './store';


test('set last price reducer action', () => {
    store.dispatch({
        type:'SET_LAST_PRICE',
        payload: {
            last_price: 5000
        }
    });
    const data = store.getState();
    expect(data.last_price).toBe(5000);
});



test('call update quote action with invalid params', () => {
    store.dispatch({
        type:'UPDATE_QUOTE',
        payload: {
            tradeAmount: 100
        }
    });
    let state = store.getState();
    expect(state.error).toBe(null);
    expect(state.tradeAmount).toBe(100);
    expect(state.quote).toBe(state.tradeAmount/state.last_price);
    store.dispatch({
        type:'UPDATE_QUOTE',
        payload: {
            tradeAmount: 7000
        }
    });

    state = store.getState();
    expect(state.error).toBe('Insufficient funds');
    expect(state.tradeAmount).toBe(7000);
    expect(state.quote).toBe(state.tradeAmount/state.last_price);


    store.dispatch({
        type:'UPDATE_QUOTE',
        payload: {
            tradeAmount: 155.123
        }
    });

    state = store.getState();
    expect(state.error).toBe(null);
    expect(state.tradeAmount).toBe(155.123);
    expect(state.quote).toBe(state.tradeAmount/state.last_price);

});


test('executing trade', () => {
    store.dispatch({
        type:'UPDATE_QUOTE',
        payload: {
            tradeAmount: 100
        }
    });

    store.dispatch({
        type: 'EXECUTE_TRADE'
    });

    let state = store.getState();
    expect(state.usd).toBe(56.12);
    expect(state.btc).toBe(0.02);
    expect(state.error).toBe(null);


    store.dispatch({
        type: 'EXECUTE_TRADE'
    });

    state = store.getState();
    expect(state.usd).toBe(56.12);
    expect(state.btc).toBe(0.02);
    expect(state.tradeAmount).toBe(100);
    expect(state.error).toBe('Insufficient funds');

    store.dispatch({
        type:'UPDATE_QUOTE',
        payload: {
            tradeAmount: 56
        }
    });


    store.dispatch({
        type: 'EXECUTE_TRADE'
    });

    state = store.getState();
    expect(state.usd).toBe(0.12);
    expect(state.btc).toBe(0.0312);
    expect(state.tradeAmount).toBe(56);
    expect(state.error).toBe(null);

});