import React, {Component} from 'react';
import './App.css';
import {connect} from 'react-redux';
import axios from 'axios';


const mapStateToProps = (state) => {
    return {
        state: state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        executeTrade: () => {
            dispatch({
                type: 'EXECUTE_TRADE',
            });
        },
        updateQuote: (payload) => {
            dispatch({
                type: 'UPDATE_QUOTE',
                payload: payload
            })
        },
        setLastPrice: (payload) => {
            dispatch({
                type: 'SET_LAST_PRICE',
                payload: payload
            })
        }
    }
};


class App extends Component {
    setLastPrice() {
        const url = 'https://api.bitfinex.com/v2/ticker/tBTCUSD';
        axios.get(url).then((response) => {
            this.props.setLastPrice(
                {
                    last_price: response.data[9]
                });
        }).catch((err) => {
            throw err;
        });
    }

    updateQuote() {
        this.props.updateQuote(
            {
                tradeAmount: document.getElementById('tradeAmount').value
            });
    }

    componentWillMount() {
        this.setLastPrice();
    }

    render() {
        const state = this.props.state;
        const textStyle = {
            fontSize: '12px',
        };

        const quote = {
            fontSize: '12px',
            color: '#aba3a3'
        };
        return (
            <div className="App">
                <div className="currencyTrade">
                    <div className="balance">
                        <span style={textStyle}>Account Balance</span>
                        <br/>
                        <span>USD: {this.props.state.usd}</span>
                        <span>BTC: {this.props.state.btc}</span>
                    </div>
                    <div className='tradeValues'>
                        <span style={textStyle}> Trade</span>
                        <span className='valueBlocks'> USD </span>
                        <input type='number' id='tradeAmount' onKeyUp={this.updateQuote.bind(this)}
                               placeholder='Enter your amount'/>
                        <span style={textStyle}> For</span>
                        <span className='valueBlocks'> BTC</span>
                        <span className='valueBlocks' style={quote}> {state.quote}</span>
                        <button className='button' onClick={() => this.props.executeTrade()}> Trade</button>
                        {state.error != null && <span> {state.error} </span>}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
